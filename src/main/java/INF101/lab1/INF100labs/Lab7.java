package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int sumRow = sumRow(grid, 0);
        int sumCol = sumCol(grid, 0);
        for (int i = 0; i< grid.size(); i++) {
            if (!(sumRow == sumRow(grid, i))) {
                return false;
            }
        }
        for (int i = 0; i< grid.get(0).size(); i++) {
            if (!(sumCol == sumCol(grid, i))) {
                return false;
            }
        }
        return true;
    }

// fra øvingstime
    public static int sumRow(ArrayList<ArrayList<Integer>> grid, Integer rowIndex) {
        int sum = 0;
        ArrayList<Integer> row = grid.get(rowIndex);
        for (Integer x : row) {
            sum += x;
        }
        return sum;
    }
// fra øvingstime
    public static int sumCol(ArrayList<ArrayList<Integer>> grid, Integer colIndex) {
        int sum = 0;
        for (int i = 0; i< grid.get(0).size(); i++){
            sum += grid.get(i).get(colIndex);
        }
        return sum;
    }

}