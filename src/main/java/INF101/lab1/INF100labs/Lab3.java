package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        crossSum(12);

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i=7; i <= n; i = i+7 ){
            System.out.println(i);
        }
    }

    public static void multiplicationTable(int n) {
        // hentet inspirasjon fra spørsmål og svar discord "Triped — 01/18/2023 8:02 PM"
        for (int i=1; i <= n; i++){
            System.out.print(i+":");
            for (int j=1; j <= n; j++){
                System.out.print(" " + i*j);
            }
            System.out.println("");
        }

    }

    public static int crossSum(int num) {
        String numToString = Integer.toString(num);
        int numChar = numToString.length();
        int crossSum = 0;
        for (int i=0; i < numChar; i++){
            char currentChar = numToString.charAt(i);
            int currentInt = Character.getNumericValue(currentChar);
            crossSum += currentInt;
        }
        //System.out.println(crossSum);
       return crossSum;
    }

}