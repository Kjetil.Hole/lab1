package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int charCount = word1.length();
        if (word2.length() > charCount) {
            charCount = word2.length();
        }
        if (word3.length() > charCount) {
            charCount = word3.length();
        }
        if (word1.length() == charCount){
            System.out.println(word1);   
        }
        
        if (word2.length() == charCount){
            System.out.println(word2);   
        }

        if (word3.length() == charCount){
            System.out.println(word3);   
        }

    }

    public static boolean isLeapYear(int year) {
        boolean leapYear = false;
        if ((year % 100 == 0) && (year % 400 == 0)) {
            leapYear = true;
        }
        else if ((year % 4 == 0) && (year % 100 == 0)) {
            leapYear = false;
        }
        else if (year % 4 == 0) {
            leapYear = true;
        }
        System.out.println(leapYear);
        return leapYear;

    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0){
            if (num % 2 == 0){
                return true;
            }
        }
        return false;
        
        
    }

}
