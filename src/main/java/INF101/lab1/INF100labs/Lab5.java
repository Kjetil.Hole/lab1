package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        // fra øvingstime
        ArrayList<Integer> newList = new ArrayList<Integer>();
        for (Integer x : list) {
            if (!(x==3)) {
                newList.add(x);
            } 
        }
        return newList;
    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> remaining =  list;
        ArrayList<Integer> uniqueValues =  new ArrayList<Integer>();
        while (!(remaining.size() == 0)) {
            int value = remaining.get(0);
            uniqueValues.add(value);

            ArrayList<Integer> newList = new ArrayList<Integer>();
            for (Integer x : remaining) {
                if (!(x==value)) {
                    newList.add(x);
                }
            }
            remaining = newList;
        }
        return uniqueValues;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        // fra øvingstime
        for (int i = 0; i < a.size(); i++) {
            int aElem = a.get(i);
            int bElem = b.get(i);
            int sum = aElem + bElem;
            a.set(i,sum);
        }

    }

}